from django.db.transaction import atomic
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from _utils import create_obj
from _utils.serializers import (
    add_to_many_field,
    update_instance_without_many_fields,
)
from customers.models import MobileOperator, Tag
from customers.serializers import (
    MobileOperatorInfoSerializer,
    TagInfoSerializer,
    MobileOperatorCreateUpdateWithParentModelSerializer,
    TagCreateUpdateWithParentModelSerializer,
)
from .message_serializers import MessageInfoSerializer

from ..models import Mailing
from ..selectors import get_customers_count_for_mailing


class MailingCreateSerializer(ModelSerializer):
    mobile_operators = MobileOperatorCreateUpdateWithParentModelSerializer(
        many=True
    )
    tags = TagCreateUpdateWithParentModelSerializer(many=True)

    class Meta:
        model = Mailing
        fields = [
            "is_stopped",
            "name",
            "started",
            "ended",
            "time_zone",
            "text",
            "mobile_operators",
            "tags",
            "uuid",
        ]

    def create(self, validated_data):
        tags_data = validated_data.pop("tags", [])
        mobile_operators_data = validated_data.pop("mobile_operators", [])

        with atomic():
            instance = create_obj(
                instance_model=self.Meta.model,
                **validated_data,
            )

            add_to_many_field(
                instance=instance,
                instance_field_name="mobile_operators",
                related_obj_model=MobileOperator,
                related_objs_data=mobile_operators_data,
            )

            add_to_many_field(
                instance=instance,
                instance_field_name="tags",
                related_obj_model=Tag,
                related_objs_data=tags_data,
            )

        return instance


class MailingUpdateSerializer(ModelSerializer):
    mobile_operators = MobileOperatorCreateUpdateWithParentModelSerializer(
        many=True
    )
    tags = TagCreateUpdateWithParentModelSerializer(many=True)

    class Meta:
        model = Mailing
        fields = [
            "is_stopped",
            "name",
            "started",
            "ended",
            "time_zone",
            "text",
            "mobile_operators",
            "tags",
            "uuid",
        ]

    def update(self, instance, validated_data):
        tags_data = validated_data.pop("tags", [])
        mobile_operators_data = validated_data.pop("mobile_operators", [])

        with atomic():
            update_instance_without_many_fields(
                instance=instance, **validated_data
            )

            add_to_many_field(
                instance=instance,
                instance_field_name="mobile_operators",
                related_obj_model=MobileOperator,
                related_objs_data=mobile_operators_data,
            )
            add_to_many_field(
                instance=instance,
                instance_field_name="tags",
                related_obj_model=Tag,
                related_objs_data=tags_data,
            )

        return instance


class MailingInfoSerializer(ModelSerializer):
    mobile_operators = MobileOperatorInfoSerializer(many=True, read_only=True)
    tags = TagInfoSerializer(many=True, read_only=True)
    customers_count = SerializerMethodField(read_only=True)
    messages_count = SerializerMethodField(read_only=True)

    @staticmethod
    def get_customers_count(obj: Mailing) -> int:
        return get_customers_count_for_mailing(mailing=obj)

    @staticmethod
    def get_messages_count(obj: Mailing) -> dict:
        all_messages = obj.messages.all()

        all_messages_count = all_messages.count()
        planned_messages_count = all_messages.filter(status="PLANNED").count()
        sent_messages_count = all_messages.filter(status="SENT").count()
        failed_messages_count = all_messages.filter(status="FAILED").count()

        message_count = {
            "all_count": all_messages_count,
            "planned_count": planned_messages_count,
            "sent_count": sent_messages_count,
            "failed_count": failed_messages_count,
        }

        return message_count

    class Meta:
        model = Mailing
        fields = [
            "is_stopped",
            "name",
            "started",
            "ended",
            "text",
            "time_zone",
            "mobile_operators",
            "tags",
            "customers_count",
            "messages_count",
            "uuid",
        ]


class MailingDetailSerializer(ModelSerializer):
    mobile_operators = MobileOperatorInfoSerializer(many=True, read_only=True)
    tags = TagInfoSerializer(many=True, read_only=True)
    messages = MessageInfoSerializer(many=True, read_only=True)

    class Meta:
        model = Mailing
        fields = "__all__"
