from rest_framework.serializers import ModelSerializer

from customers.serializers import CustomerMiniInfoSerializer
from ..models import Message, Mailing


class MessageInfoSerializer(ModelSerializer):
    customer = CustomerMiniInfoSerializer(read_only=True)

    class Meta:
        model = Message
        fields = [
            "customer",
            "created",
            "sent",
            "failed",
            "status",
            "uuid",
        ]


class MessageDetailSerializer(ModelSerializer):
    class MailingMiniInfoSerializer(ModelSerializer):
        class Meta:
            model = Mailing
            fields = ["name", "uuid"]

    customer = CustomerMiniInfoSerializer(read_only=True)
    mailing = MailingMiniInfoSerializer(read_only=True)

    class Meta:
        model = Message
        fields = "__all__"
