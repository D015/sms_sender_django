from django.db.models import (
    Model,
    TextField,
    DateTimeField,
    ManyToManyField,
    CharField,
    BooleanField,
)

from _config.settings import TIME_ZONE
from _utils import AbstractBaseModel, time_zone_validator
from customers.models import MobileOperator, Tag


class Mailing(AbstractBaseModel, Model):
    last_sent = DateTimeField(blank=True, null=True)
    is_stopped = BooleanField(default=False)
    name = CharField(blank=True, max_length=120)
    started = DateTimeField(blank=True, null=True)
    ended = DateTimeField(blank=True, null=True)
    time_zone = CharField(
        max_length=120, default=TIME_ZONE, validators=[time_zone_validator]
    )
    text = TextField()
    mobile_operators = ManyToManyField(
        MobileOperator, related_name="mailings", blank=True
    )
    tags = ManyToManyField(Tag, related_name="mailings", blank=True)

    class Meta:
        ordering = ["started"]
