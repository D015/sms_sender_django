from datetime import datetime

from django.db.models import (
    DateTimeField,
    CharField,
    ForeignKey,
    PROTECT,
    TextChoices,
    BigAutoField,
    PositiveBigIntegerField,
)

from django.db.models.fields import checks

from _utils import AbstractBaseModel
from customers.models import Customer
from .mailing_models import Mailing


class MessageStatus(TextChoices):
    PLANNED = "PLANNED"
    SENT = "SENT"
    FAILED = "FAILED"


def increment_message():
    if not Message.objects.exists():
        return 1
    last_message = Message.objects.all().order_by("-send_id").first()
    next_message_number = last_message.send_id + 1 if last_message else 1
    return next_message_number


class Message(AbstractBaseModel):
    send_id = PositiveBigIntegerField(
        db_index=True, unique=True, editable=False, default=increment_message
    )

    sent = DateTimeField(blank=True, null=True)
    failed = DateTimeField(blank=True, null=True)
    status = CharField(
        max_length=8,
        choices=MessageStatus.choices,
        default=MessageStatus.PLANNED,
    )
    mailing = ForeignKey(Mailing, related_name="messages", on_delete=PROTECT)
    customer = ForeignKey(Customer, related_name="messages", on_delete=PROTECT)

    class Meta:
        ordering = ["created"]
