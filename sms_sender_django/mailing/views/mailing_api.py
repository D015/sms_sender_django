from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView

from ..serializers.mailing_serializers import (
    MailingCreateSerializer,
    MailingDetailSerializer,
)
from ..models import Mailing
from ..serializers.mailing_serializers import (
    MailingInfoSerializer,
    MailingUpdateSerializer,
)


class MailingCreateAPIView(CreateAPIView):
    serializer_class = MailingCreateSerializer
    queryset = Mailing.objects.all()


class MailingUpdateAPIView(UpdateAPIView):
    serializer_class = MailingUpdateSerializer
    queryset = Mailing.objects.all()


class MailingListAPIView(ListAPIView):
    serializer_class = MailingInfoSerializer

    def get_queryset(
        self,
    ):
        return Mailing.objects.all().prefetch_related("messages")


class MailingDetailAPIView(APIView):
    serializer_class = MailingDetailSerializer

    def get_queryset(self):
        return Mailing.objects.all().prefetch_related("messages")
