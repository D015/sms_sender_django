from mailing.views.mailing_api import (
    MailingCreateAPIView,
    MailingListAPIView,
    MailingDetailAPIView,
)
