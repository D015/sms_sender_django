from django.db.models import QuerySet, Q
from datetime import datetime, timedelta

from _config.settings import CELERY_TASK_TIME_LIMIT
from _utils.date_time import get_datetime_tz_from_datetime_utc
from customers.models import Customer
from ..models import Mailing, MessageStatus, Message


def get_customers_for_mailing_time_utc(
    mailing: Mailing, datetime_utc: datetime = datetime.utcnow()
) -> QuerySet[Customer]:

    customers = (
        Customer.objects.filter(
            mobile_operator__in=mailing.mobile_operators.all(),
            tags__in=mailing.tags.all(),
        )
        .distinct("uuid")
        .exclude(
            messages__status=MessageStatus.SENT, messages__mailing=mailing
        )
    )

    return customers


def get_customers_count_for_mailing(mailing: Mailing) -> int:
    customers = (
        Customer.objects.filter(
            mobile_operator__in=mailing.mobile_operators.all(),
            tags__in=mailing.tags.all(),
        )
        .distinct("uuid")
        .count()
    )
    return customers


def get_mailings_to_send_for_datetime_utc(
    send_datetime_utc: datetime = datetime.utcnow(),
) -> QuerySet[Mailing]:
    mailings_to_send = Mailing.objects.filter(
        started__lte=get_datetime_tz_from_datetime_utc(
            datetime_utc=send_datetime_utc, time_zone=Mailing.time_zone
        ),  # TODO VVV
        ended__gte=(
            get_datetime_tz_from_datetime_utc(
                datetime_utc=send_datetime_utc, time_zone=Mailing.time_zone
            )
            + timedelta(seconds=CELERY_TASK_TIME_LIMIT)
        ),
        stopped=False,
    ).order_by("ended")

    return mailings_to_send
