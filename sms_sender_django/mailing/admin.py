from django.contrib.admin import ModelAdmin, register

from .models import Mailing, Message


@register(Mailing)
class MailingAdmin(ModelAdmin):
    filter_horizontal = ["tags"]


@register(Message)
class MessageAdmin(ModelAdmin):
    pass
