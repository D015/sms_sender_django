from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import (
    MailingCreateAPIView,
    MailingDetailAPIView,
    MailingListAPIView,
)


urlpatterns = [
    path("", MailingListAPIView.as_view()),
    path("create", MailingCreateAPIView.as_view()),
    path("<uuid:mailing_uuid>", MailingDetailAPIView.as_view()),
]

# urlpatterns = format_suffix_patterns(urlpatterns)
