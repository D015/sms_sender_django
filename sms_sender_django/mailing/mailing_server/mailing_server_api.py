import logging
from datetime import datetime
from pytz import timezone

import requests

from celery import shared_task

from django.db.models import QuerySet

from _utils.constants import CUSTOMER_BATCH_SIZE, SEND_IP_TOKEN, SEND_IP_URL
from customers.models import Customer
from .checkers import is_utc_now_in_available_time_tz_period
from ..models import Message, Mailing, MessageStatus
from ..selectors import get_customers_for_mailing_time_utc
from ..selectors.mailing_selectors import get_mailings_to_send_for_datetime_utc


logger = logging.getLogger(__name__)


def send_message(send_id: int, phone: int, text: str):
    response = requests.post(
        f"{SEND_IP_URL}{send_id}",
        headers={
            "Authorization": f"Bearer {SEND_IP_TOKEN}",
            "Content-Type": "application/json",
            "accept": "application/json",
        },
        json={"id": send_id, "phone": phone, text: text},
    )

    return response.status_code


@shared_task
def send_to_customers(customers: QuerySet[Customer], mailing: Mailing) -> None:
    for customer in customers:
        message, _ = Message.objects.get_or_create(
            customers=customer, mailing=mailing
        )
        if is_utc_now_in_available_time_tz_period(
            available_start_tz=customer.available_start_tz,
            available_end_tz=customer.available_start_tz,
            time_zone=timezone(customer.time_zone),
        ):
            try:
                customer_phone_number_int = int(customer.phone_number)
            except ValueError:
                logger.debug(f"{customer.phone_number} is invalid number")
                continue
            else:
                status_code = send_message(
                    send_id=message.send_id,
                    phone=customer_phone_number_int,
                    text=mailing.text,
                )
            if status_code == 200:
                message.status = MessageStatus.SENT
            else:
                message.status = MessageStatus.FAILED
            message.save()


def get_batches(customers: QuerySet, customer_batch_size: int) -> tuple:
    customer_list = []
    for i in range(0, customers.count(), customer_batch_size):
        customer_list.append(customers[i : i + customer_batch_size])
    return tuple(customer_list)


def send_mailings(
    time_now: datetime = datetime.utcnow(),
    customer_batch_size=CUSTOMER_BATCH_SIZE,
) -> None:
    mailings_to_send = get_mailings_to_send_for_datetime_utc(
        send_datetime_utc=time_now
    )
    for mailing_to_send in mailings_to_send:
        customers = get_customers_for_mailing_time_utc(mailing=mailing_to_send)
        customer_batches = get_batches(customers, customer_batch_size)
        for customer_batch in customer_batches:
            send_to_customers.delay(
                customers=customer_batch, mailing=mailing_to_send
            )
