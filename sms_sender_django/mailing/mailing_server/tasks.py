from celery import shared_task
from django.core.mail import send_mail
from jinja2 import Template

from mailing.mailing_server.mailing_server_api import send_mailings


@shared_task
def task_send_mailings() -> None:
    send_mailings()


@shared_task
def task_send_email() -> None:
    with open("mailing/mail_templates/message.txt") as file:
        template = Template(file.read())
        text = template.render(name="User")
    # send_mail()
