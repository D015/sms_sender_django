from datetime import time
from pytz import timezone

from _utils.date_time import get_time_tz_from_datetime_utc


def is_utc_now_in_available_time_tz_period(
    available_start_tz: time,
    available_end_tz: time,
    time_zone: timezone,
) -> bool:

    time_now_tz = get_time_tz_from_datetime_utc(time_zone=time_zone)

    # For a period in one day
    if (
        available_start_tz < available_end_tz
        and available_start_tz < time_now_tz < available_end_tz
    ):
        return True

    # For a period in different days
    elif available_start_tz > available_end_tz and (
        available_start_tz < time_now_tz or time_now_tz < available_end_tz
    ):
        return True

    return False
