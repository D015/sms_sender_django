import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "_config.settings")

app = Celery("proj_1")

app.config_from_object("django.conf:settings", namespace="CELERY")

app.autodiscover_tasks()

REPETITION_TIME = 60 * 3.5
app.conf.beat_schedule = {
    "send_mailings_every_3.5_min": {
        "task": "mailing.tasks.task_send_mailings",
        "schedule": REPETITION_TIME,
    },
    "send_mail_per_day": {"task": ..., "schedule": crontab(hour=0, minute=0)},
}
app.conf.timezone = "UTC"
