from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveDestroyAPIView,
    UpdateAPIView,
)

from ..models import Tag
from ..serializers import (
    TagInfoSerializer,
    TagCreateSerializer,
    TagDetailSerializer,
    TagUpdateSerializer,
)


class TagCreateAPIView(CreateAPIView):
    serializer_class = TagCreateSerializer
    queryset = Tag.objects.all()


class TagUpdateAPIView(UpdateAPIView):
    serializer_class = TagUpdateSerializer
    queryset = Tag.objects.all()


class TagListAPIView(ListAPIView):
    serializer_class = TagInfoSerializer
    queryset = Tag.objects.all()


class TagRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    serializer_class = TagDetailSerializer
    queryset = Tag.objects.all()
