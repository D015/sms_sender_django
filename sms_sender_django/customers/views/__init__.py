from .mobile_operator_api import (
    MobileOperatorCreateAPIView,
    MobileOperatorUpdateAPIView,
    MobileOperatorListAPIView,
    MobileOperatorRetrieveDestroyAPIView,
)

from .tag_api import (
    TagCreateAPIView,
    TagUpdateAPIView,
    TagListAPIView,
    TagRetrieveDestroyAPIView,
)

from .customer_api import (
    CustomerCreateAPIView,
    CustomerUpdateAPIView,
    CustomerListAPIView,
    CustomerRetrieveDestroyAPIView,
)
