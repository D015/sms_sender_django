from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveDestroyAPIView,
)

from ..models import MobileOperator
from ..serializers import (
    MobileOperatorInfoSerializer,
    MobileOperatorCreateSerializer,
    MobileOperatorUpdateSerializer,
    MobileOperatorDetailSerializer,
)


class MobileOperatorCreateAPIView(CreateAPIView):
    serializer_class = MobileOperatorCreateSerializer
    queryset = MobileOperator.objects.all()


class MobileOperatorUpdateAPIView(UpdateAPIView):
    serializer_class = MobileOperatorUpdateSerializer
    queryset = MobileOperator.objects.all()


class MobileOperatorListAPIView(ListAPIView):
    serializer_class = MobileOperatorInfoSerializer
    queryset = MobileOperator.objects.all()


class MobileOperatorRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    serializer_class = MobileOperatorDetailSerializer
    queryset = MobileOperator.objects.all()
