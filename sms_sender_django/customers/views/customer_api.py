from rest_framework.generics import (
    ListAPIView,
    UpdateAPIView,
    RetrieveDestroyAPIView,
    CreateAPIView,
)

from ..models import Customer
from ..serializers import (
    CustomerInfoSerializer,
    CustomerCreateSerializer,
    CustomerUpdateSerializer,
    CustomerDetailSerializer,
)


class CustomerCreateAPIView(CreateAPIView):
    serializer_class = CustomerCreateSerializer
    queryset = Customer.objects.all()


class CustomerUpdateAPIView(UpdateAPIView):
    serializer_class = CustomerUpdateSerializer
    queryset = Customer.objects.all()


class CustomerListAPIView(ListAPIView):
    serializer_class = CustomerInfoSerializer
    queryset = Customer.objects.all()


class CustomerRetrieveDestroyAPIView(RetrieveDestroyAPIView):
    serializer_class = CustomerDetailSerializer
    queryset = Customer.objects.all()
