from .tag_serializers import (
    TagInfoSerializer,
    TagUpdateSerializer,
    TagCreateSerializer,
    TagDetailSerializer,
    TagCreateUpdateWithParentModelSerializer,
)

from .mobile_operator_serializers import (
    MobileOperatorCreateSerializer,
    MobileOperatorInfoSerializer,
    MobileOperatorUpdateSerializer,
    MobileOperatorDetailSerializer,
    MobileOperatorCreateUpdateWithParentModelSerializer,
)

from .customer_serializers import (
    CustomerUpdateSerializer,
    CustomerInfoSerializer,
    CustomerCreateSerializer,
    CustomerDetailSerializer,
    CustomerMiniInfoSerializer,
)
