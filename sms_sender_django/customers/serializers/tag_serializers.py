from rest_framework.serializers import ModelSerializer, CharField
from ..models import Tag


class TagCreateSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = ["name", "uuid"]


class TagCreateUpdateWithParentModelSerializer(ModelSerializer):
    name = CharField()

    class Meta:
        model = Tag
        fields = ["name", "uuid"]


class TagUpdateSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = ["name"]


class TagInfoSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = ["name", "uuid"]


class TagDetailSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"
