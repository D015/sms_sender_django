from rest_framework.serializers import ModelSerializer, CharField

from ..models import MobileOperator


class MobileOperatorCreateSerializer(ModelSerializer):
    class Meta:
        model = MobileOperator
        fields = ["code", "uuid"]


class MobileOperatorCreateUpdateWithParentModelSerializer(ModelSerializer):
    code = CharField()

    class Meta:
        model = MobileOperator
        fields = ["code", "uuid"]


class MobileOperatorUpdateSerializer(ModelSerializer):
    class Meta:
        model = MobileOperator
        fields = ["code"]


class MobileOperatorInfoSerializer(ModelSerializer):
    class Meta:
        model = MobileOperator
        fields = ["code", "uuid"]


class MobileOperatorDetailSerializer(ModelSerializer):
    class Meta:
        model = MobileOperator
        fields = "__all__"
