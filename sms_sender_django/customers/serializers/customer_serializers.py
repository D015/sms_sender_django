from django.db.transaction import atomic
from rest_framework.serializers import ModelSerializer, CharField

from _utils import create_obj
from _utils.serializers import (
    add_to_many_field,
    update_instance_without_many_fields,
)
from .tag_serializers import TagInfoSerializer

from .mobile_operator_serializers import (
    MobileOperatorCreateUpdateWithParentModelSerializer,
    MobileOperatorInfoSerializer,
)
from .tag_serializers import TagCreateUpdateWithParentModelSerializer

from ..models import Customer, Tag, MobileOperator


class CustomerCreateSerializer(ModelSerializer):
    mobile_operator = MobileOperatorCreateUpdateWithParentModelSerializer()
    tags = TagCreateUpdateWithParentModelSerializer(many=True)
    uuid = CharField(read_only=True)

    class Meta:
        model = Customer
        fields = [
            "phone_number",
            "mobile_operator",
            "tags",
            "time_zone",
            "uuid",
        ]

    def create(self, validated_data):
        tags_data = validated_data.pop("tags", [])

        # mobile_operator_data blank=False
        mobile_operator_data = validated_data.pop("mobile_operator")

        with atomic():
            mobile_operator, _ = MobileOperator.objects.get_or_create(
                **mobile_operator_data
            )

            instance = create_obj(
                instance_model=self.Meta.model,
                mobile_operator=mobile_operator,
                **validated_data,
            )

            add_to_many_field(
                instance=instance,
                instance_field_name="tags",
                related_obj_model=Tag,
                related_objs_data=tags_data,
            )

        return instance


class CustomerUpdateSerializer(ModelSerializer):
    mobile_operator = MobileOperatorCreateUpdateWithParentModelSerializer()
    tags = TagCreateUpdateWithParentModelSerializer(many=True)

    class Meta:
        model = Customer
        fields = [
            "phone_number",
            "mobile_operator",
            "tags",
            "time_zone",
            "uuid",
        ]

    def update(self, instance, validated_data):
        tags_data = validated_data.pop("tags", [])
        # mobile_operator_data blank=False
        mobile_operator_data = validated_data.pop("mobile_operator")

        with atomic():
            mobile_operator, _ = MobileOperator.objects.get_or_create(
                **mobile_operator_data
            )

            update_instance_without_many_fields(
                instance=instance,
                mobile_operator=mobile_operator,
                **validated_data,
            )

            add_to_many_field(
                instance=instance,
                instance_field_name="tags",
                related_obj_model=Tag,
                related_objs_data=tags_data,
                update=True,
            )

        return instance


class CustomerInfoSerializer(ModelSerializer):
    mobile_operator = MobileOperatorInfoSerializer(read_only=True)
    tags = TagInfoSerializer(many=True)

    class Meta:
        model = Customer
        fields = [
            "phone_number",
            "mobile_operator",
            "tags",
            "time_zone",
            "uuid",
        ]


class CustomerDetailSerializer(ModelSerializer):
    mobile_operator = MobileOperatorInfoSerializer(read_only=True)
    tags = TagInfoSerializer(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = "__all__"


class CustomerMiniInfoSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = [
            "phone_number",
            "uuid",
        ]
