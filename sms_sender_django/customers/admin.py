from django.contrib import admin

from .models import Customer, MobileOperator, Tag


class CustomerAdmin(admin.ModelAdmin):
    pass


class MobileOperatorAdmin(admin.ModelAdmin):
    pass


class TagAdmin(admin.ModelAdmin):
    pass


admin.site.register(Customer, CustomerAdmin)
admin.site.register(MobileOperator, MobileOperatorAdmin)
admin.site.register(Tag, TagAdmin)
