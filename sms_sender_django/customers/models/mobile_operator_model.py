from django.db.models import CharField

from _utils import AbstractBaseModel


class MobileOperator(AbstractBaseModel):
    code = CharField(max_length=120, unique=True, db_index=True)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ["created"]
