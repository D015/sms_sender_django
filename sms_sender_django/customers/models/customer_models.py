from datetime import time

from django.db.models import (
    CharField,
    ForeignKey,
    PROTECT,
    ManyToManyField,
    TimeField,
)

from _utils import (
    AbstractBaseModel,
    phone_number_validator,
    time_zone_validator,
)
from .mobile_operator_model import MobileOperator
from .tag_models import Tag


class Customer(AbstractBaseModel):
    phone_number = CharField(
        db_index=True,
        max_length=11,
        unique=True,
        validators=[phone_number_validator],
    )

    mobile_operator = ForeignKey(
        MobileOperator, related_name="customers", on_delete=PROTECT
    )

    tags = ManyToManyField(Tag, related_name="customers", blank=True)
    time_zone = CharField(max_length=120, validators=[time_zone_validator])

    available_start_tz = TimeField(blank=True, default=time.min)
    available_end_tz = TimeField(blank=True, default=time.max)

    def __str__(self):
        return self.phone_number

    class Meta:
        ordering = ["created"]
