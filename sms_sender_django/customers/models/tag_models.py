from django.db.models import CharField

from _utils import AbstractBaseModel


class Tag(AbstractBaseModel):
    name = CharField(max_length=120, unique=True, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["created"]
