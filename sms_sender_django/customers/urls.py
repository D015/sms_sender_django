from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import (
    CustomerCreateAPIView,
    CustomerListAPIView,
    MobileOperatorCreateAPIView,
    MobileOperatorListAPIView,
    TagCreateAPIView,
    TagListAPIView,
    TagRetrieveDestroyAPIView,
    TagUpdateAPIView,
    MobileOperatorRetrieveDestroyAPIView,
    MobileOperatorUpdateAPIView,
    CustomerRetrieveDestroyAPIView,
    CustomerUpdateAPIView,
)

mobile_operator_urlpatterns = [
    path(
        "mobile_operator/<uuid:pk>/",
        MobileOperatorRetrieveDestroyAPIView.as_view(),
    ),
    path("mobile_operator/create/", MobileOperatorCreateAPIView.as_view()),
    path(
        "mobile_operator/update/<uuid:pk>/",
        MobileOperatorUpdateAPIView.as_view(),
    ),
    path("mobile_operator/", MobileOperatorListAPIView.as_view()),
]

tag_urlpatterns = [
    path("tag/<uuid:pk>/", TagRetrieveDestroyAPIView.as_view()),
    path("tag/create/", TagCreateAPIView.as_view()),
    path("tag/update/<uuid:pk>/", TagUpdateAPIView.as_view()),
    path("tag/", TagListAPIView.as_view()),
]


customer_urlpatterns = [
    path("<uuid:pk>/", CustomerRetrieveDestroyAPIView.as_view()),
    path("create/", CustomerCreateAPIView.as_view()),
    path("update/<uuid:pk>/", CustomerUpdateAPIView.as_view()),
    path("", CustomerListAPIView.as_view()),
]

urlpatterns = [
    *mobile_operator_urlpatterns,
    *tag_urlpatterns,
    *customer_urlpatterns,
]
