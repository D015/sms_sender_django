from typing import Type

from django.db.models import Model


def get_or_create_obj(model: Type[Model], **kwargs) -> tuple[Model, bool]:
    obj, is_crated = model.objects.get_or_create(**kwargs)
    return obj, is_crated


def create_obj(model: Type[Model], **kwargs) -> Model:
    return model.objects.create(**kwargs)
