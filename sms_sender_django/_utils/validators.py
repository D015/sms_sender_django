from re import compile
from pytz import all_timezones_set

from django.core.exceptions import ValidationError


def phone_number_validator(phone_number) -> None:  # TODO: Add test
    pattern = compile("^(7)[0-9]{10}$")
    if pattern.match(phone_number) is None:
        raise ValidationError("phone number is invalid")


def time_zone_validator(time_zone) -> None:  # TODO: Add test
    if time_zone not in all_timezones_set:
        raise ValidationError("time zone is invalid")
