from datetime import datetime, time
from pytz import timezone


def get_datetime_tz_from_datetime_utc(
    time_zone: timezone,
    datetime_utc: datetime = datetime.utcnow(),
) -> datetime:

    tz_timedelta = time_zone.utcoffset(datetime_utc)
    datetime_tz = datetime_utc + tz_timedelta
    return datetime_tz


def get_time_tz_from_datetime_utc(
    time_zone: timezone,
    datetime_utc: datetime = datetime.utcnow(),
) -> time:

    tz_timedelta = time_zone.utcoffset(datetime_utc)
    datetime_tz = datetime_utc + tz_timedelta
    return datetime_tz.time()


def get_datetime_utc_from_datetime_tz(
    ime_zone: timezone,
    datetime_tz: datetime,
) -> datetime:

    tz_timedelta = ime_zone.utcoffset(datetime_tz)
    datetime_utc = datetime_tz - tz_timedelta
    return datetime_utc


def get_time_utc_from_datetime_tz(
    ime_zone: timezone,
    datetime_tz: datetime,
) -> time:

    tz_timedelta = ime_zone.utcoffset(datetime_tz)
    datetime_utc = datetime_tz - tz_timedelta
    return datetime_utc.time()
