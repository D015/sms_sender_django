from typing import OrderedDict, Type

from django.db.models import Model

from .services import get_or_create_obj, create_obj


def add_to_many_field(
    instance: Model,
    instance_field_name: str,
    related_obj_model: Type[Model],
    related_objs_data: list[OrderedDict],
    update: bool = False,
) -> None:
    objs = []
    for obj_data in related_objs_data:
        obj, _ = get_or_create_obj(model=related_obj_model, **obj_data)
        objs.append(obj)
    if objs:
        if update:
            getattr(instance, instance_field_name).clear()
        getattr(instance, instance_field_name).add(*objs)


def update_instance_without_many_fields(
    instance: Model, **instance_fields_data
) -> None:
    for key_i, value_i in instance_fields_data.items():
        setattr(instance, key_i, value_i)
    instance.save()
