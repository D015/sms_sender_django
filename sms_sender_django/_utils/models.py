from datetime import datetime

from uuid import uuid4

from django.contrib.auth.models import User
from django.db.models import (
    Model,
    UUIDField,
    DateTimeField,
    BooleanField,
    ForeignKey,
    SET_NULL,
)


class AbstractBaseModel(Model):
    uuid = UUIDField(
        primary_key=True, unique=True, default=uuid4, editable=False
    )

    author = ForeignKey(User, blank=True, null=True, on_delete=SET_NULL)
    created = DateTimeField(default=datetime.utcnow, editable=False)
    updated = DateTimeField(default=datetime.utcnow)
    is_active = BooleanField(default=True)
    is_archived = BooleanField(default=False)

    def __str__(self):
        return str(self.uuid)

    class Meta:
        abstract = True
